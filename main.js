(function () {
    self.Board = function (width,height) {
    this.width=width;
    this.height=height;
    this.playing=false;
    this.game_over=false;
    this.bars=[];
    this.ball=null;
    this.playing = false;
    this.points =0;
  }

  self.Board.prototype = {
    get elements(){
        var elements = this.bars.map(function (bar) { return bar;});
        elements.push(this.ball);
        return elements;
    },
    show_points: function () {
      console.log("points: " + this.points )
    }
  }
})();

(function () {
  self.Ball=function (x,y,radius,board) {
    this.x= x;
    this.y= y;
    this.radius = radius;
    this.speed_y = 0;
    this.speed_x = 3;
    this.board = board;
    this.direction = 1;
    this.bounce_angle = 0;
    this.max_bounce_angle= Math.PI/12;
    this.speed =3;
    this.all_collisions = 0;

    board.ball = this;
    this.kind = "circle";
  }

  self.Ball.prototype = {
    move: function () {
      this.x += (this.speed_x * this.direction);
      this.y += (this.speed_y);
      if (this.x < 0 || this.x>=this.board.width) {
        board.playing = !board.playing;
        alert("Game Over. Score: " + this.board.points + " Points");
        location.reload();
      }
    },
    get width(){
      return this.radius*2;
    },
    get height(){
      return this.radius*2;
    },
    collision: function(bar){
      // react at collision and change the angle

      var relative_intersect_y = ( bar.y + (bar.height / 2) ) - this.y;

			var normalized_intersect_y = relative_intersect_y / (bar.height / 2);

			this.bounce_angle = normalized_intersect_y * this.max_bounce_angle;
			console.log(this.bounce_angle);
			this.speed_y = this.speed * -Math.sin(this.bounce_angle);
			this.speed_x = this.speed * Math.cos(this.bounce_angle);
      this.all_collisions +=1;
      // graduate difficulty
      if (this.all_collisions%6 ==0) {
        if(this.speed < 9 && this.speed_x < 9){
          this.speed_x +=1;
          this.speed +=1;
        }
      }

			if(this.x > (this.board.width / 2)) this.direction = -1;
      else this.direction = 1;
    }
  }
})();

(function () {
  self.Bar = function (x,y,width,height,board) {
    this.x=x;
    this.y=y;
    this.width=width;
    this.height=height;
    this.board=board;
    this.board.bars.push(this);
    this.kind = "rectangle";
    this.speed = 10;
  }

  self.Bar.prototype={
    down: function () {
      this.y += this.speed;

    },
    up: function () {
      this.y -= this.speed;

    },
    toString: function () {
      return "x: "+ this.x + "y: " + this.y;
    }
  }
})();

(function () {
  self.BoardView = function(canvas, board){
    this.canvas = canvas;
    this.canvas.width=board.width;
    this.canvas.height= board.height;
    this.board = board;
    this.ctx = canvas.getContext("2d");

  }

  self.BoardView.prototype = {
    clean: function () {
      this.ctx.clearRect(0,0,this.board.width,this.board.height);
    },
    draw: function () {
      for (var i = this.board.elements.length -1; i >= 0; i--){
        var el= this.board.elements[i];
        draw(this.ctx,el);
      }
    },

    check_collisions: function () {
      for (var i = this.board.bars.length -1 ; i >= 0; i--) {
        var bar = this.board.bars[i];
        if (hit(bar, this.board.ball)) {
          this.board.ball.collision(bar);
          this.board.points +=20;
        }
      };
    },
    play: function () {
      if (this.board.playing) {
        this.clean();
        this.draw();
        this.check_collisions();
        this.board.ball.move();
        document.getElementById("points").innerHTML ="Points: " + this.board.points;
      }
    }
  }

  function hit(a,b) {
    //check if a collisions with b
    var hit = false;
		//horizontal
		if(b.x + b.width >= a.x && b.x < a.x + a.width)
		{
			//vertical
			if(b.y + b.height >= a.y && b.y < a.y + a.height)
				hit = true;
		}
		//a con b
		if(b.x <= a.x && b.x + b.width >= a.x + a.width)
		{
			if(b.y <= a.y && b.y + b.height >= a.y + a.height)
				hit = true;
		}
		//b con a
		if(a.x <= b.x && a.x + a.width >= b.x + b.width)
		{
			if(a.y <= b.y && a.y + a.height >= b.y + b.height)
				hit = true;
		}

    return hit;

  }

  function draw(ctx,element) {
    switch (element.kind) {
      case "rectangle":
        ctx.fillRect(element.x,element.y,element.width,element.height);
        break;
      case "circle":
        ctx.beginPath();
        ctx.arc(element.x, element.y, element.radius, 0, 7);
        ctx.fill();
        ctx.closePath();
        break;
    }
  }
})();

var board = new Board(800, 400);
var bar_2= new Bar(20,100,20,100, board);
var bar = new Bar(760,100,20,100, board);
var bar_sup= new Bar (0,0,800,3, board);
var bar_inf = new Bar(0,397,800,3, board);
var canvas = document.getElementById('canvas');
var board_view = new BoardView(canvas, board);
var ball = new Ball(400,100,10,board);


document.addEventListener("keydown",function(ev){ //you can get code key with a print console.log(ev.keyCode);

  if (ev.keyCode==38) {
    ev.defaultPrevented;
    bar.up();
  }

  else if (ev.keyCode==40) {
    ev.defaultPrevented;
    bar.down();
  }

  else if (ev.keyCode==87) {
    ev.defaultPrevented;
    bar_2.up();
  }

  else if (ev.keyCode==83) {
    ev.defaultPrevented;
    bar_2.down();
  }
  else if(ev.keyCode==32){
    ev.defaultPrevented;
    board.playing = !board.playing;
  }
});
board_view.draw();
window.requestAnimationFrame(controller);

function controller() {
  board_view.play();
  window.requestAnimationFrame(controller);
}
